const express = require('express')
const { sequelize }  = require('./src/models');
import cors from 'cors'
const initRouter = require('./src/routes')
require('dotenv').config()

const app = express()
app.use(cors({
    origin: process.env.CLIENT_URL,
    methods: ['GET','POST','PUT','DELETE']
}))

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

initRouter(app)
//chạy cái này để đông bộ database ko cần chạy lệnh npx sequelize-cli db:migrate
if (process.env['NODE_ENV'] === 'production') {
    sequelize.sync()
} else if (process.env['NODE_ENV'] === 'development') {
    sequelize.sync({ alter: true }) //thay đổi tên column
    //sequelize.sync({ force: true }) // xóa databse tạo bảng mới
}

const PORT = process.env.PORT || 5001

const listener = app.listen(PORT, () => {
    console.log(`Server is running on the port ${PORT}`)
})