const userRouter = require('./userRouter')
const authRouter = require('./auth')
const { notFound } = require('../middlewares/handleErrors')

const initRouter = (app) => {
    app.use('/api/v1/user', userRouter)
    app.use('/api/v1/auth', authRouter)

    app.use(notFound)
}

module.exports = initRouter