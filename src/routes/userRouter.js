const router = require('express').Router();
const controllers  = require('../controller')
import verifyToken from '../middlewares/verifyToken';
//import { isAdmin, isUser } from '../middlewares/verifyRoles';

//Public Routes
//tang middlewares xu ly o giua truoc khi truy cap vao route nhan 3 tham so req, res, next() ham next dung de next qua buoc tiep theo
//Private Routes
router.use(verifyToken)//middlewares
//router.use(isUser)//middlewares
//router.get('/', [verifyToken,isUser],controllers.getCurrent) Cach2 viet truc tiep cho route can su dung middlewares
router.get('/', controllers.getCurrent)

module.exports = router