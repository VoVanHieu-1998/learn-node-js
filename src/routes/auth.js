import * as controllers from '../controller'
import validation from '../middlewares/validationMiddleware'
import { register, login } from '../validates/defaultValidate'
const router = require('express').Router()

router.post('/register' ,validation(register) ,controllers.register)
router.post('/login' , validation(login), controllers.login)

module.exports = router