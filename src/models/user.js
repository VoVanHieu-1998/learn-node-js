'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // User.belongsTo(models.Role, {
      //   foreignKey: 'role_code',
      //   targetKey: 'code', // mapping code been bang role voi cot role_code ben foreingkey
      //   as: 'roleData'
      // }) 
      this.belongsTo(models.Role,{ foreignKey: { name: 'role_id' }, sourceKey: 'role_id', targetKey: 'id'});
    }
  }
  User.init({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: {
      type: DataTypes.STRING,
      allowNull: true
    },
    avatar: DataTypes.STRING,
    role_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User',
    tableName: 'users',
    timestamps: false
  });
  return User;
};