import Joi from "joi";

export const email = Joi.string().pattern(new RegExp('[a-z0-9]+@[a-z]+\.[a-z]{2,3}')).required()
export const password = Joi.string()