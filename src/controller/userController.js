import { internalServerError, badRequest } from "../middlewares/handleErrors"
import * as userService from '../service'

export const getCurrent = async (req, res) => {
    try {
        const {id, role_id} = req.user
        const response = await userService.getOneUser(id, role_id)
        return res.status(200).json(response)
    } catch (error) {
        return internalServerError(res);
    }
}