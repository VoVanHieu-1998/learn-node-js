import * as serviceAuth from "../service"
import { internalServerError, badRequest } from "../middlewares/handleErrors"
import { email, password } from "../helpers/joiSchemas"

export const register = async (req, res) => {
    try {
        const {email, password} = req.body
        const response = await serviceAuth.register(email, password)
        return res.status(200).json(response)
    } catch (error) {
        return internalServerError(res);
    }
}

export const login = async (req, res) => {
    try {
        const {email, password} = req.body
        const response = await serviceAuth.login(email,password)
        return res.status(200).json(response)
    } catch (error) {
        return internalServerError(res);
    }
}