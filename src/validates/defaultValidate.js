const yup = require('yup');

exports.register = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().min(8).required()
});
exports.login = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().required()
});
