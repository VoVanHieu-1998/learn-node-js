import Jwt from "jsonwebtoken";
import { badRequest, notAuth } from "./handleErrors";

const verifyToken = (req, res, next) => {
    const token = req.headers.authorization
    if(!token) return notAuth('Require authorization', res)
    const accessToken = token.split(' ')[1]
    Jwt.verify(accessToken, process.env.JWT_SECRET, (err, user)=>{
        if(err) return notAuth('Access token may be expired or invalid', res)

        req.user = user
        return next()

    })
}

export default verifyToken;