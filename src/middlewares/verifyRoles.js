import { notAuth } from "./handleErrors"

export const isAdmin = (req, res, next) => {
    const { role_code } = req.user
    if(role_code !== 'R1') return notAuth('Require role Admin', res)
    return next()
}
export const isUser = (req, res, next) => {
    const { role_code } = req.user
    if(role_code !== 'R2') return notAuth('Require role User', res)
    return next()
}