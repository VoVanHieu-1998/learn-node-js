import db from '../models'
const { Op } = require('sequelize');

export const getOneUser = (userId,roleId) => new Promise( async (resolve, reject) => {
    try {
        const query_role = [];
        if (roleId) query_role.push({ id: roleId });
        const response = await db.User.findOne({
            where: { id:userId },
            attributes : {
                exclude : ['password']
            },
            // include: [
            //     {model: db.Role}
            // ],
            include: [
                {
                //   where: {
                //     [Op.and]: [...query_role],
                //   },
                  model: db.Role,
                  attributes: ['id', 'code', 'value'],
                },
              ]
        })
        resolve({
            err : response ? 0 : 1,
            mes: response ? 'Get Success' : 'Get fail',
            data: response
        })
    } catch (error) {
        reject(error)
    }
})