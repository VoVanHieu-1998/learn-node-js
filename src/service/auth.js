import db from '../models'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'

const hashPassword = password => bcrypt.hashSync(password, bcrypt.genSaltSync(8))

export const register = (email, password) => new Promise( async (resolve, reject) => {
    try {
        //[user, created] data response nhan dc ham nay created: boolean, user la data
        const response = await db.User.findOrCreate({
            where: { email },
            defaults: {
                email,
                password : hashPassword(password),
                role_id : 1
            }
        })
        //1: data can ma hoa
        const token = response[1] ? jwt.sign({id: response[0].id, email: response[0].email, role_id: response[0].role_id}, process.env.JWT_SECRET, {expiresIn: '5d'}): null 
        resolve({
            err : response[1] ? 0 : 1,
            mes: response[1] ? 'Register OK' : 'Email is used',
            acccess_token : token ? `Bearer ${token}` : null
        })
    } catch (error) {
        reject(error)
    }
})

export const login = (email, password) => new Promise( async (resolve, reject) => {
    try {
        //[user, created] data response nhan dc ham nay created: boolean, user la data
        const response = await db.User.findOne({
            where: { email },
            raw: true // ko lay instance sequelize
        })
        const isChecked = response && bcrypt.compareSync(password, response.password)
        const token = isChecked ? jwt.sign({id: response.id, email: response.email, role_id: response.role_id}, process.env.JWT_SECRET, {expiresIn: process.env.EXPIRESIN}): null 
        
        resolve({
            err : token ? 0 : 1,
            mes: token ? 'Login OK' : response ? 'Password is wrong' : 'Email has been registered',
            acccess_token : token ? `Bearer ${token}` : null
        })
    } catch (error) {
        reject(error)
    }
})